"use strict";

module.exports = function() {
	const api = this;
	const config = api.config;

	const logDebug = api.log( "scrippted:config:debug" );

	let defaultPrinterInfo = "scrippted";

	class ScripptedPrinterConfiguration {
		/**
		 * Adjusts printer information included with either printer's
		 * configuration.
		 *
		 * @param {string} info printer information to use on future queries for printer attributes
		 * @returns {void}
		 */
		static setDefaultPrinterInfo( info ) {
			if ( info && String( info ).trim() !== "" ) {
				defaultPrinterInfo = String( info );
			}
		}

		/**
		 * Generates basic configuration of printer selected by its name based
		 * on runtime configuration if available or on printer's name otherwise.
		 *
		 * @param printerName name of printer to configure
		 * @returns {object} configuration of named printer
		 */
		static getByName( printerName ) {
			const helper = api.runtime.services.ScripptedHelper;
			let source;

			if ( config.printers ) {
				source = config.printers[printerName];

				logDebug( "custom printer configuration for %s: %j", printerName, source );
			}

			if ( typeof source !== "object" || !source ) {
				source = {};
			}

			// create normalized set of printer information from printer's configuration or name
			const isPCL = source.pcl == null ? helper.isPclPrinter( printerName ) : Boolean( source.pcl );
			const isPS = source.postscript == null ? helper.isPostscriptPrinter( printerName ) : Boolean( source.postscript );

			if ( isPCL && isPS ) {
				throw new TypeError( `invalid configuration for printer ${printerName}: must be either PCL, Postscript or PDF printer` );
			}

			const format = isPS ? "postscript" : isPCL ? "pcl" : "pdf";
			const mimes = [];
			let model = source.model != null ? String( source.model ) : null;

			switch ( format ) {
				case "pdf" :
					if ( !model ) {
						model = "Microsoft Print To PDF";
					}

					mimes.push( "application/pdf" );
					mimes.push( "application/x-pdf" );
					break;

				case "postscript" :
					if ( !model ) {
						model = "Microsoft PS Class Driver";
					}

					mimes.push( "application/postscript" );
					mimes.push( "application/x-postscript" );
					break;

				case "pcl" :
					if ( !model ) {
						model = "Microsoft PCL6 Class Driver";
					}

					mimes.push( "application/x-pcl" );
					mimes.push( "application/vnd.hp-pcl" );
					break;
			}

			const supportsColors = !( source.blackAndWhite == null ? helper.isBlackAndWhitePrinter( printerName ) : Boolean( source.blackAndWhite ) );

			return {
				info: ( source.info && String( source.info ) ) || defaultPrinterInfo,
				model,
				format,
				mimes,
				supportsColors,
			};
		}
	}

	return ScripptedPrinterConfiguration;
};
