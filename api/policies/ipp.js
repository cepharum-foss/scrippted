/**
 * (c) 2020 cepharum GmbH, Berlin, http://cepharum.de
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2020 cepharum GmbH
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * @author: cepharum
 */

"use strict";

const { IPPMessage, Status } = require( "@cepharum/ipp" );

module.exports = function() {
	const api = this;
	const { services } = api.runtime;

	/**
	 * Implements policies for commonly handling IPP messages.
	 */
	class IppPolicies {
		/**
		 * Rejects request unless it is IPP-related.
		 *
		 * @param {Hitchy.Core.IncomingMessage} req
		 * @param {Hitchy.Core.ServerResponse} res
		 * @param {function} next
		 * @returns {void}
		 */
		static requireIpp( req, res, next ) {
			let response;

			if ( this.local.ipp ) {
				switch ( this.local.ipp.message.version ) {
					case "1.0" :
					case "1.1" :
						next();
						return;
				}

				response = this.local.ipp.message.deriveResponse( Status.serverErrorVersionNotSupported );
			} else {
				response = IPPMessage.createRequest( 0x8fff )
					.deriveResponse( Status.clientErrorBadRequest )
					.setOperationAttribute( "status-text", "not an IPP request" );
			}

			res
				.set( "content-type", "application/ipp" )
				.end( response.toBuffer() );
		}

		/**
		 * Logs IPP information on current request for debugging purposes.
		 *
		 * @param {Hitchy.Core.IncomingMessage} req request descriptor
		 * @param {Hitchy.Core.ServerResponse} res response manager
		 * @param {function} next callback to signal policy done processing
		 * @returns {void}
		 */
		static logIpp( req, res, next ) {
			if ( req.context.local.ipp ) {
				services.ScripptedLog.logIpp( req.context.local.ipp.message );
			}

			next();
		}
	}

	return IppPolicies;
};
