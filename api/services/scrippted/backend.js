/**
 * (c) 2021 cepharum GmbH, Berlin, http://cepharum.de
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2021 cepharum GmbH
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * @author: cepharum
 */

"use strict";

const {
	TypeNameWithoutLanguage,
	TypeNoValue,
	TypeMimeMediaType, TypeInteger, IPPMessage, TypeUri, TypeEnum, TypeKeyword
} = require( "@cepharum/ipp" );

module.exports = function() {
	const api = this;
	const { services } = api.runtime;

	let latestJobId = 0;

	/**
	 * Implements default backend supporting scrippted in processing selected
	 * requests.
	 *
	 * Using a separate backend in addition to the controller(s) is meant to
	 * simplify customized implementations based on scrippted.
	 */
	class ScripptedBackend {
		/**
		 * Prints a job on selected printer.
		 *
		 * @param {Hitchy.Core.IncomingMessage} req request descriptor
		 * @param {Hitchy.Core.ServerResponse} res response manager
		 * @param {IPPMessage} ipp IPP request message
		 * @param {Readable} data stream of data following IPP message in request
		 * @returns {Promise<IPPMessage>} promises IPP response message describing result of printing job
		 */
		static printJob( req, res, ipp, data ) {
			return this.dropRequestData( data )
				.then( () => {
					const jobId = ++latestJobId;
					const myUrl = new URL( req.context.url.toString() );

					myUrl.search = "";
					myUrl.pathname = myUrl.pathname.replace( new RegExp( `(/${req.params.printer})(/.*)?$` ), "$1" ) + "/" + jobId;

					return ipp.deriveResponse()
						.setJobAttribute( "job-id", new TypeInteger( jobId ) )
						.setJobAttribute( "job-uri", new TypeUri( String( myUrl ) ) )
						.setJobAttribute( "job-state", new TypeEnum( 9 ) ) // completed -> make it vanish from client's queue
						.setJobAttribute( "job-state-reason", new TypeKeyword( "none" ) );
				} );
		}

		/**
		 * Lists jobs of selected printer.
		 *
		 * @param {Hitchy.Core.IncomingMessage} req request descriptor
		 * @param {Hitchy.Core.ServerResponse} res response manager
		 * @param {IPPMessage} ipp IPP request message
		 * @param {Readable} data stream of data following IPP message in request
		 * @returns {Promise<IPPMessage>} promises IPP response message describing result of printing job
		 */
		static listJobs( req, res, ipp, data ) {
			return this.dropRequestData( data )
				.then( () => ipp.deriveResponse() );
		}

		/**
		 * Cancels selected job.
		 *
		 * @param {Hitchy.Core.IncomingMessage} req request descriptor
		 * @param {Hitchy.Core.ServerResponse} res response manager
		 * @param {IPPMessage} ipp IPP request message
		 * @param {Readable} data stream of data following IPP message in request
		 * @returns {Promise<IPPMessage>} promises IPP response message describing result of printing job
		 */
		static cancelJob( req, res, ipp, data ) {
			return this.dropRequestData( data )
				.then( () => ipp.deriveResponse() );
		}

		/**
		 * Drops any data sent with request without processing it in any way,
		 * but waits for all data being received successfully.
		 *
		 * @param {Readable} data stream of octets following IPP message in a request
		 * @returns {Promise<void>} promises all delivered data seen and dropped successfully
		 * @protected
		 */
		static dropRequestData( data ) {
			return new Promise( ( resolve, reject ) => {
				data.once( "end", resolve );
				data.once( "error", reject );
				data.resume();
			} );
		}

		/**
		 * Extracts basic information from provided IPP message sent to print a
		 * job.
		 *
		 * @param {IPPMessage} ipp IPP message requesting job to be printed
		 * @returns {{jobName: string, documentFormat: string, userName: string}} extracted meta information
		 * @protected
		 */
		static extractBasicMetaFromPrintIpp( ipp ) {
			const { operation } = ipp.attributes;

			const jobName = operation.get( "job-name", new TypeNameWithoutLanguage( "job" ) );
			const userName = operation.get( "requesting-user-name", new TypeNoValue() );
			const documentFormat = operation.get( "document-format", new TypeMimeMediaType( "application/pdf" ) );

			return { jobName, userName, documentFormat };
		}
	}

	return ScripptedBackend;
};
