const initializeFunction = require( "./initialize" );

module.exports = function( ...args ) {
	const api = this;

	return {
		initialize() {
			initializeFunction.call( api, ...args );
		},
	};
}
