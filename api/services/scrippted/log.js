/**
 * (c) 2021 cepharum GmbH, Berlin, http://cepharum.de
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2021 cepharum GmbH
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * @author: cepharum
 */

"use strict";

const { IPPMessage } = require( "@cepharum/ipp" );

module.exports = function() {
	const api = this;

	const logInfo = api.log( "scrippted:ipp:info" );
	const logDebug = api.log( "scrippted:ipp:debug" );

	/**
	 * Implements commonly useful logging handlers.
	 */
	class ScripptedLog {
		/**
		 * Logs provided IPP message.
		 *
		 * @param {IPPMessage} ipp IPP message to log
		 * @param {boolean} response set true if message is describing a response instead of a request
		 * @returns {void}
		 */
		static logIpp( ipp, response = false ) {
			const { version, code, id, attributes } = ipp;

			if ( response ) {
				logInfo( "got IPP v%s response %s w/ ID %d", version, code, id );
			} else {
				logInfo( "got IPP v%s request %s w/ ID %d", version, IPPMessage.getOperationName( code ), id );
			}

			Object.keys( attributes )
				.forEach( groupName => {
					const group = attributes[groupName];

					if ( group.size > 0 ) {
						for ( const [ key, values ] of group ) {
							logDebug( " - %s: %s (%s)", groupName, key,
								Array.isArray( values ) ? values.map( v => String( v ) ).join( ", " ) : String( values ) );
						}
					}
				} );
		}
	}

	return ScripptedLog;
}
