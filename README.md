# scrippted

a Javascript-based IPP service

## License

MIT

## Install

```bash
npm install -g @cepharum/scrippted
```

## Usage

Run service by invoking

```bash
scrippted
```

This service is dropping print jobs on reception. See the [project ipp-stub](https://gitlab.com/cepharum-foss/ipp-stub) relying on this one for an example on how to actually process incoming print jobs.