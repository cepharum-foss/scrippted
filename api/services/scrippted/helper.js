/**
 * (c) 2021 cepharum GmbH, Berlin, http://cepharum.de
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2021 cepharum GmbH
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * @author: cepharum
 */

"use strict";

module.exports = function() {
	class ScripptedHelper {
		/**
		 * Detects if named printer is assumed to accept PCL-encoded jobs instead of
		 * PDF documents.
		 *
		 * @param {string} printerName name of printer to inspect
		 * @returns {boolean} true if named printer is assumed to accept PCL-encoded jobs
		 */
		static isPclPrinter( printerName ) {
			return /\bpcl\b/i.test( printerName );
		}

		/**
		 * Detects if named printer is assumed to accept PS-encoded jobs instead
		 * of PDF documents.
		 *
		 * @param {string} printerName name of printer to inspect
		 * @returns {boolean} true if named printer is assumed to accept PS-encoded jobs
		 */
		static isPostscriptPrinter( printerName ) {
			return /\bps\b/i.test( printerName );
		}

		/**
		 * Detects if named printer is assumed to be capable of black-and-white prints,
		 * only.
		 *
		 * @param {string} printerName name of printer to inspect
		 * @returns {boolean} true if named printer is capable of printing black-and-white, only
		 */
		static isBlackAndWhitePrinter( printerName ) {
			return /\bbw\b/i.test( printerName );
		}
	}

	return ScripptedHelper;
};
