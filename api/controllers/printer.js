/**
 * (c) 2020 cepharum GmbH, Berlin, http://cepharum.de
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2020 cepharum GmbH
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * @author: cepharum
 */

"use strict";

const {
	Operation, Status,
	TypeKeyword, TypeMimeMediaType, TypeNaturalLanguage, TypeEnum,
	EnumOperations, EnumPrinterStates,
} = require( "@cepharum/ipp" );

const start = Date.now();

const HttpToIppStatus = {
	400: Status.clientErrorBadRequest,
	401: Status.clientErrorNotAuthenticated,
	403: Status.clientErrorForbidden,
	404: Status.clientErrorNotFound,
	406: Status.clientErrorNotPossible,
	408: Status.clientErrorTimeout,
	410: Status.clientErrorGone,
	413: Status.clientErrorRequestEntityTooLarge,
	415: Status.clientErrorDocumentFormatNotSupported,
};

module.exports = function() {
	/** @type {HitchyAPI} */
	const api = this;
	const { services } = api.runtime;

	const logDebug = api.log( "scrippted:ipp:debug" );
	const logError = api.log( "scrippted:ipp:error" );

	class PrinterController {
		/**
		 * Handles request for printer-related action.
		 *
		 * @param {Hitchy.Core.IncomingMessage} req request descriptor
		 * @param {Hitchy.Core.ServerResponse} res response manager
		 * @returns {Promise<void>} promises request being handled
		 */
		static handleRequest( req, res ) {
			/** @type {IPPMessage} */
			const { message, data } = req.context.local.ipp;
			let response;

			switch ( message.code ) {
				case Operation.GetPrinterAttributes :
					response = this.describePrinter( req, res, message );
					break;

				case Operation.PrintJob :
					response = services.ScripptedBackend.printJob( req, res, message, data );
					break;

				case Operation.GetJobs :
					response = services.ScripptedBackend.listJobs( req, res, message, data );
					break;

				case Operation.CancelJob :
					response = services.ScripptedBackend.cancelJob( req, res, message, data );
					break;

				default :
					response = message.deriveResponse( Status.serverErrorOperationNotSupported,
						`unsupported operation: 0x${( "000" + message.code.toString( 16 )).slice( -4 )}` );
			}

			return Promise.resolve( response )
				.catch( error => {
					const { statusCode, message } = error;
					const ippStatus = HttpToIppStatus[statusCode] || ( statusCode >= 0x400 ? statusCode : Status.serverErrorInternalError );

					logError( error );

					return message.deriveResponse( ippStatus, message );
				} )
				.then( _response => {
					res.set( "content-type", "application/ipp" )
						.end( _response.toBuffer() );

					services.ScripptedLog.logIpp( _response, true );
				} );
		}

		/**
		 * Handles request for attributes of a printer.
		 *
		 * @param {Hitchy.Core.IncomingMessage} req request descriptor
		 * @param {Hitchy.Core.ServerResponse} res response manager
		 * @param {IPPMessage} ipp IPP request message
		 * @returns {IPPMessage} IPP response message
		 */
		static describePrinter( req, res, ipp ) {
			const { ScripptedPrinterConfiguration: config } = this.services;
			const printerName = req.params.printer;
			const printer = config.getByName( printerName );

			logDebug( "configuration of printer %s is %j", printerName, printer );

			const response = ipp.deriveResponse()
				.setPrinterAttribute( "charset-configured", "utf-8" )
				.setPrinterAttribute( "charset-supported", "utf-8" )
				.setPrinterAttribute( "color-supported", printer.supportsColors )
				.setPrinterAttribute( "compression-supported", new TypeKeyword( "none" ) )
				.setPrinterAttribute( "generated-natural-language-supported", new TypeNaturalLanguage( "en" ) )
				.setPrinterAttribute( "ipp-versions-supported", new TypeKeyword( "1.0" ) )
				.setPrinterAttribute( "ipp-versions-supported", new TypeKeyword( "1.1" ), true )
				.setPrinterAttribute( "multiple-document-jobs-supported", false )
				.setPrinterAttribute( "natural-language-configured", new TypeNaturalLanguage( "en" ) )
				.setPrinterAttribute( "operations-supported", new TypeEnum( "Print-Job", EnumOperations ) )
				.setPrinterAttribute( "operations-supported", new TypeEnum( "Validate-Job", EnumOperations ), true )
				.setPrinterAttribute( "operations-supported", new TypeEnum( "Get-Printer-Attributes", EnumOperations ), true )
				.setPrinterAttribute( "operations-supported", new TypeEnum( "Get-Jobs", EnumOperations ), true )
				.setPrinterAttribute( "operations-supported", new TypeEnum( "Cancel-Job", EnumOperations ), true )
				.setPrinterAttribute( "operations-supported", new TypeEnum( "Get-Job-Attributes", EnumOperations ), true )
				.setPrinterAttribute( "pdl-override-supported", false )
				.setPrinterAttribute( "printer-info", printer.info )
				.setPrinterAttribute( "printer-name", printerName )
				.setPrinterAttribute( "printer-is-accepting-jobs", true )
				.setPrinterAttribute( "printer-state", new TypeEnum( "idle", EnumPrinterStates ) )
				.setPrinterAttribute( "printer-state-reasons", new TypeKeyword( "none" ) )
				.setPrinterAttribute( "printer-up-time", Math.floor( ( Date.now() - start ) / 1000 ) )
				.setPrinterAttribute( "printer-uri-supported", ipp.attributes.operation.get( "printer-uri" ) )
				.setPrinterAttribute( "queued-job-count", 0 )
				.setPrinterAttribute( "uri-authentication-supported", new TypeKeyword( "requesting-user-name" ) )
				.setPrinterAttribute( "uri-security-supported", new TypeKeyword( "none" ) )
				.setPrinterAttribute( "printer-make-and-model", printer.model )
				.setPrinterAttribute( "document-format-default", new TypeMimeMediaType( printer.mimes[0] ) );

			for ( let i = 0; i < printer.mimes.length; i++ ) {
				response.setPrinterAttribute( "document-format-supported", new TypeMimeMediaType( printer.mimes[i] ), i > 0 )
			}

			return response;
		}
	}

	return PrinterController;
};
